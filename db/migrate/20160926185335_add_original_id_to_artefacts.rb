class AddOriginalIdToArtefacts < ActiveRecord::Migration[5.0]
  def change
    change_table :artefacts do |t|
      t.string :original_id
    end
  end
end
