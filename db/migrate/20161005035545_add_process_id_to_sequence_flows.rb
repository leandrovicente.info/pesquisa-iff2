class AddProcessIdToSequenceFlows < ActiveRecord::Migration[5.0]
  def change
    change_table :sequence_flows do |t|
      t.integer :bpmn_process_id
    end
  end
end
