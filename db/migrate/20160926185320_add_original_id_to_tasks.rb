class AddOriginalIdToTasks < ActiveRecord::Migration[5.0]
  def change
    change_table :tasks do |t|
      t.string :original_id
    end
  end
end
