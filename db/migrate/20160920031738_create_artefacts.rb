class CreateArtefacts < ActiveRecord::Migration[5.0]
  def change
    create_table :artefacts do |t|
      t.string :name
      t.integer :task_id

      t.timestamps
    end
  end
end
