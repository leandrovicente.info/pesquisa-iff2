class CreateSequenceFlows < ActiveRecord::Migration[5.0]
  def change
    create_table :sequence_flows do |t|
      t.string :original_id
      t.string :sourceRef
      t.string :targetRef

      t.timestamps
    end
  end
end
