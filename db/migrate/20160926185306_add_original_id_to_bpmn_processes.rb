class AddOriginalIdToBpmnProcesses < ActiveRecord::Migration[5.0]
  def change
    change_table :bpmn_processes do |t|
      t.string :original_id
    end
  end
end
