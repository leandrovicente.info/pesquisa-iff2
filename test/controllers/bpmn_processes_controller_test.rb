require 'test_helper'

class BpmnProcessesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @bpmn_process = bpmn_processes(:one)
  end

  test "should get index" do
    get bpmn_processes_url
    assert_response :success
  end

  test "should get new" do
    get new_bpmn_process_url
    assert_response :success
  end

  test "should create bpmn_process" do
    assert_difference('BpmnProcess.count') do
      post bpmn_processes_url, params: { bpmn_process: { name: @bpmn_process.name } }
    end

    assert_redirected_to bpmn_process_url(BpmnProcess.last)
  end

  test "should show bpmn_process" do
    get bpmn_process_url(@bpmn_process)
    assert_response :success
  end

  test "should get edit" do
    get edit_bpmn_process_url(@bpmn_process)
    assert_response :success
  end

  test "should update bpmn_process" do
    patch bpmn_process_url(@bpmn_process), params: { bpmn_process: { name: @bpmn_process.name } }
    assert_redirected_to bpmn_process_url(@bpmn_process)
  end

  test "should destroy bpmn_process" do
    assert_difference('BpmnProcess.count', -1) do
      delete bpmn_process_url(@bpmn_process)
    end

    assert_redirected_to bpmn_processes_url
  end
end
