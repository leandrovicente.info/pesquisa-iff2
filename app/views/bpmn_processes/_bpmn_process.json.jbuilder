json.extract! bpmn_process, :id, :name, :created_at, :updated_at
json.url bpmn_process_url(bpmn_process, format: :json)