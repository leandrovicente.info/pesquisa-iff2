json.extract! artefact, :id, :name, :task_id, :created_at, :updated_at
json.url artefact_url(artefact, format: :json)