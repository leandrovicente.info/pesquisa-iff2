class Task < ApplicationRecord
  belongs_to :bpmn_process
  has_many :artefacts, :dependent => :destroy

  #Looks for activities that are "sourceRef" in sequences where this task is found. In other words, activities that are executed before this one
  def incoming
    find_sequences = SequenceFlow.where(targetRef: self[:original_id])
    if find_sequences
      sequences = find_sequences.map{ |sequence| sequence[:sourceRef]}
      return Task.where(original_id: sequences)
    end
  end

#Looks for activities that are "targetRef" in sequences where this task is found. In other words, activities that are executed after this one
  def outgoing
    find_sequences = SequenceFlow.where(sourceRef: self[:original_id])
    if find_sequences
      sequences = find_sequences.map{ |sequence| sequence[:targetRef]}
      return Task.where(original_id: sequences)
    end
  end
end
