class BpmnProcess < ApplicationRecord
  has_many :tasks, :dependent => :destroy
  has_many :sequence_flows, :dependent => :destroy

end
