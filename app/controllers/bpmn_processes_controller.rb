class BpmnProcessesController < ApplicationController
  require "rexml/document"
  include REXML
  before_action :set_bpmn_process, only: [:show, :edit, :update, :destroy]

  # GET /bpmn_processes
  # GET /bpmn_processes.json
  def index
    @bpmn_processes = BpmnProcess.all
  end

  # GET /bpmn_processes/1
  # GET /bpmn_processes/1.json
  def show
  end

  # GET /bpmn_processes/new
  def new

  end

  # GET /bpmn_processes/1/edit
  def edit
  end

  # POST /bpmn_processes
  # POST /bpmn_processes.json
  def create

    begin
      file = File.new(params[:file].path)
      doc = Document.new file

      #Goes through the whole bpmn file looking for the desired data hierarchically
      doc.elements.each("*/process") { |process|

        #Gets all artefacts of this process
        artefacts = []
        process.elements.each('dataObject') {|artefact| artefacts << Artefact.new(name: artefact.attributes['name'], original_id: artefact.attributes['id']) }

        #Gets all associatins of this process
        associations = []
        process.elements.each('association') {|association| associations << {sourceRef: association.attributes['sourceRef'], targetRef: association.attributes['targetRef']}}

        new_process =  BpmnProcess.new(name: process.attributes['name'] == nil ? "undefined" : process.attributes['name'], original_id: process.attributes['id'])

        #Goes through every task in the file
        process.elements.each('task') { |task|

          new_task = Task.new(name: task.attributes['name'] == nil ? "undefined" : task.attributes['name'] , original_id: task.attributes['id'])

          #Looks for all associations of this task
          task_associations = associations.find_all {|a| a[:sourceRef] == new_task.original_id }
          if (task_associations)
            #If associations were found, then it creates an array with all target references from them (target_refs)
            target_refs = task_associations.map { |association| association[:targetRef] }
            #It also creates an array of artefacts for those artefacts that have their original id as any of the target references numbers
            task_artefacts = artefacts.find_all {|a| target_refs.include? a.original_id }
            if (task_artefacts)
              #If artefacts were found for this task, then it will be included as its children
              new_task.artefacts = task_artefacts
            end
          end

          #includes this task in the process to be saved later
          new_process.tasks << new_task

          }

        #Looks for sequences flows to determine the execution order of each activity later
        process.elements.each('sequenceFlow') {|sequenceFlow|
          new_sequence = SequenceFlow.new(original_id: sequenceFlow.attributes['id'], sourceRef: sequenceFlow.attributes['sourceRef'], targetRef: sequenceFlow.attributes['targetRef'])
          new_process.sequence_flows << new_sequence
        }
          #Finally saves the process, its tasks (and artefacts) in the database
        new_process.save
      }
      flash[:notice] = "BPMn was successfully imported"
      redirect_to bpmn_processes_url
    rescue
      flash[:alert] = "Failed to import BPMn"
      redirect_to bpmn_processes_url
    end
  end

  # PATCH/PUT /bpmn_processes/1
  # PATCH/PUT /bpmn_processes/1.json
  def update
    respond_to do |format|
      if @bpmn_process.update(bpmn_process_params)
        format.html { redirect_to @bpmn_process, notice: 'Bpmn process was successfully updated.' }
        format.json { render :show, status: :ok, location: @bpmn_process }
      else
        format.html { render :edit }
        format.json { render json: @bpmn_process.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bpmn_processes/1
  # DELETE /bpmn_processes/1.json
  def destroy
    @bpmn_process.destroy
    respond_to do |format|
      format.html { redirect_to bpmn_processes_url, notice: 'Bpmn process was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bpmn_process
      @bpmn_process = BpmnProcess.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bpmn_process_params
      params.require(:bpmn_process).permit(:name)
    end
end
