class ReportController < ApplicationController
  def index
    processes = BpmnProcess.all

    report = ""

    processes.each do |process|
      report << "Process ID: #{process.original_id}\n"
      report << "Process Name: #{process.name}"
      process.tasks.each do |task|
        report << "\n\n\tTask ID: #{task.original_id}\n"
        report << "\tTask Name: #{task.name}\n"
        report << "\n\tIncoming tasks: "
         task.incoming.each do |t| report << "#{t.name}; " end
        report << "\n\tOutgoing tasks: "
         task.outgoing.each do |t| report << "#{t.name}; " end

        task.artefacts.each do |artefact|
          report << "\n\n\t\tArtefact ID: #{artefact.original_id}\n"
          report << "\t\tArtefact Name: #{artefact.name}\n\n"
        end
        report << "\n -------------------------------------------- \n"
      end
      report << "\n==================================================\n\n"
    end

    send_data(
      report,
      :type => 'text',
      :filename => 'newlog.txt',
      :disposition => 'attachment'
    )

  end
end
