Rails.application.routes.draw do
  root to: "home#index"
  resources :artefacts
  resources :tasks
  resources :bpmn_processes
  get :uploads, :controller => :uploads, :action => :index
  get :report, to: 'report#index', as: :report
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
